Please answer the below Data Structure Questions:

why do we need data structure?
Ans : We need it to use memory more efficiently and time sufficiently.

What are the various operations that can be performed on different Data Structures?
Ans : Insert, delete, sort and etc.

List out the areas in which data structures are applied extensively?
Ans :

what is an Array?
Ans : An collection of values stored in memory that can be called upon using indexes.

what is the difference between the Array and ArrayList?
Ans : Array has a set number of indexes, ArrayList doesn't and is more flexible.

what is LinkedList?
Ans : An collection of values stored in memory that can be called upon using indexes like arrayList.

How is an Array different from Linked List?
Ans : Array has set number of indexes, linkedList doesnt. Also linkedList is easier for manipulating the data.

what is queue?
Ans : A queue is a linear structure that follows FIFO.

what is stack?
Ans : Stack is also a data structure like queue but it follows FIFO and LIFO.

what is FIFO and LIFO?
Ans : FIFO is First In First Out, LIFO is Last In Last Out.

what is the order of complexity?
Ans :

what is the best case to search an element from an array?
Ans : The best case of the unsorted array is O(n).

what is the worst case to search an element from an array?
Ans : The worst case of the unsorted array is also O(n).

what is tree in data structure?
Ans : A tree is a hierarchical data structure defined as a collection of nodes. Nodes represent value and nodes are connected by edges.
    Linear btw.

what is graph in data structure?
Ans : A Graph is a non-linear data structure consisting of nodes and edges.

what is the difference between the HashTable and HashMap?
Ans : HashMap allows one null key and multiple null values whereas Hashtable doesn’t allow any null key or value.
    Hashtable is thread-safe and can be shared with many threads, hashmap is not.
    HashMap is non-synchronized, hashtable is synchronized.

What are the major data structures used in the following areas : RDBMS, Network data model and Hierarchical data model.
Ans:RDBMS- array
Network data model-Graph
Hierarchical data model- Trees

How HashMap works in java?
Ans: We can store value with a key and call that value using the key in a hashMap.

What is ArrayIndexOutOfBoundsException in java? When it occurs?
Ans : Its an exception that occurs when a Java program tries to access an invalid index.

What are the different ways of copying an array into another array?
Ans :

What is difference between an array and a linked list?
Ans : Array has set number of indexes, linkedList doesnt. Also linkedList is easier for manipulating the data.

What is DFS and BFS?
Ans : BFS stands for Breadth First Search. It uses a queue data structure and finds the shortest path in graph.
    DFS stands for Depth First Search. It uses the stack data structure.

What is Recursion?
Ans : Recursion is when a problem's solution depends on the solution of the other problems with the same instances.

What are linear and non linear data Structures?
Ans : Linear data structure is a data structure where data elements are arranged sequentially or linearly.
    Non linear data structure is a data structure where data elements are not arranged sequentially or linearly.

What is Big-(O)-notation?
Ans : Big O notation describes the complexity of a code using algebraic terms.