Please answer the below core-java Questions:

What is programing language?
Ans : A programming language is a computer language programmers use to develop software programs, scripts, or other sets of instructions for computers to execute.

What jdk contains or composed of and define each components?
Ans : It contains
 - jre(java runtime environment) : JRE acts as a kind of translator and facilitator between the Java program and the OS.
 - javac : It's a compiler. It converts source code into Java bytecode.
 - java : It’s a loader that works for all the java applications.
 - jar: It is an archiver. It puts class libraries packages into one JAR file. It also manages those files.
 - javadoc: It is a documentation generator, which produces documentation from source code comments, automatically.
 - jabswitch: It is a Java Access Bridge.
 - idlj: An IDL-to-Java compiler is used to generate Java bindings from the provided Java IDL file.

What is IDE?
Ans : Java IDE (Integrated Development Environment) is a software application that enables users to write and debug Java programs more easily.

What are the IDEs available in the market to support java?
Ans : IntelliJ, eclipse, xcode are the most popular ones around the globe.

Explain the entire java life cycle.
Ans : After creating a java class, the java compiler compiles the material inside of it, and creates a text file of that said class
   and turns it into a byte code. It gets sent to the jvm where the code gets executed per instruction.

what is class?
Ans : where all the information and codes are stored. We define objects and etc in a class.

What is object?
Ans : It's an instance of a class. Each object has it's own behavior, they work different to one another.

What is the entry point of an application?
Ans : It identifies a resource that is an access point to an application.

Why main is static?
Ans : Its static so that the compiler can run it without creating an object.

Which class is the superclass of all classes?
Ans : Object class.

What is difference between path and classpath variables?
Ans : Path variables are used to set the path for java softaware tools such as javac.exe. And classpath variables are used to set the path for java classes.

What is the difference between an Interface and Abstract class?
Ans : Interface is not a class, while Abstract class is, even though it can have characteristics of an interface inside of it.

Can you make an Object from an Interface and Abstract class ? if not how do you use it ?
Ans : No, but we can implement and extend the said interface and abstract class to another class
    and use objects for that new class to call our interface and abstract class.

what is Access Specifier?
Ans : It specifies which classes can access a given class and its fields, constructors and methods.

What is OOP ? Define each feature of java OOP.
Ans : Object-oriented programming (OOP) is a computer programming model that organizes
    software design around data, or objects, rather than functions and logic.
    Enscapsulation - data hiding--> getter,setter.
    Inheritance: lets you extend classes such as a parent class to a child class.
    Polymorphism: having many forms.
    Abstraction : calling objects, classes and variables that represent more complex underlying code and data.

What is Java Package and which package is imported by default?
Ans : A package in Java is used to group related classes. Java compiler imports java.lang package internally by default.

What is API? Name list of API that you have used so far.
Ans : API (application programming interface) is a set of definitions and protocols for building and integrating application software.
    APIs for java would be java program, java compiler, Java API and others.
    So far I've used classes, and interfaces.

Does java support multiple inheritance, explain why or why not?
Ans : No because it can cause the java compiler to come accross a situation where there would be multiple same named methods,
    and it won't know which to prioratize.

What is method overloading and when it happens?
Ans : When theres two methods with the same name in the same class but with different parameters.

Explain exceptions in java and how to handle it.
Ans : In java sometimes while running the code it may find some error before executing, it's called exceptions.
    We handle exceptions by using try and catch.

What is static keyword in java? How it has been used in variables and methods ?
Ans : Its mainly used for memory management. Its used to share the same variable or method of a given class.

What is final and how it has been used variables and methods?
Ans : Using the final keyword means its value cannot be modified.
    Using final in a variable means that variable cant be changed later on, its set.
    Using final in a method also means there wont be any method overriding, no change to the method.

What is final, finally and finalize?
Ans : Finally is the block of code that will execute no matter what in the try & catch,
    Finalize is the method that collects an unused object,
    and final means its value cant be changed.

What is a constructor ?
Ans : A constructor in Java is a special method that is used to initialize objects of another class to call its non-static methods and variables.

Can we have multiple constructors in a class?
Ans : Yes, but with different parameters.

If we don't have a constructor declared, what is called during the object creation?
Ans : Default.

What is "this" keyword in java ?
Ans : It refers to the current object or constructor.

What is "super" keyword in java? How many possible way can you use?
Ans : It is used to call method from a parent class.
    It is normally used to differentiate between parent class and child class that have same method name.
    It can be used 3 ways: 1. to refer immediate parent class instance variable.
                           2. to invoke immediate parent class method.
                           3. to invoke immediate parent class constructor.

What is JVM stand for ?
Ans : Java virtual machine.

Is JVM platform independent?
Ans : Yes.

What version of java are you using?
Ans : 17.0.1

What is JAR stand for ?
Ans : Java archive.

What is the difference between JDK and JVM?
Ans : JDK is a software development kit that provides necessary libraries to run java code while JVM executes them in byte code.

What is the difference between JVM and JRE?
Ans : Java Runtime Environment contains JVM. In other words JRE is the box and JVM is the content of the box.

What is compile time and run time?
Ans : Compile time is when the code is converted into machine code.
      Runtime is when the code is converted to machine code and now going to run and execute.

What is heap?
Ans : Heap is a memory storage where all the things are stored in. It is created when jvm starts up.

How java manage it's memory?
Ans : Java itself manages the memory and needs no explicit intervention of the programmer.
    Java has a garbage collector within itself that ensures that the unused space gets cleaned and memory can be freed when not needed.

What is the difference between String, StringBuffer and StringBuilder?
Ans : String can be many things, but mostly its a class that stores all sorts of Strings.
      StringBuffer and StringBuilder are classes that are used to manipulate a String.
      StringBuilder is faster than String buffer when it comes to performing but StringBuffer is thread safe and synchronized.

What is Singleton class?
Ans : Its a class that can have only one object at a time so that we dont have to keep creating objects and waste space.

What is Serialization and Deserialization?
Ans : Serialization converts an in-memory data structure to a value that can be stored or transferred.
    Deserialization takes a series of bytes and converts it to an in-memory data structure that can be consumed programmatically.

when to use transient variable in java?
Ans : We use it for any non-serializable references.

Difference between while and do..while loop?
Ans : while loops is executing the body of the loop only if the condition is met
    do while loop is executing even if the while condition is given or not.

What is Enum?
Ans : A type of class where we store value that are not changeable.

What is Iterator?
Ans :

Which one will take more memory, an int or Integer?

Why is String Immutable in Java?

What is constructor chaining in Java?

The difference between Serial and Parallel Garbage Collector?
What is JIT stands for?
Explain Java Heap space and Garbage collection?
Can you guarantee the garbage collection process?
What is the difference between stack and heap in Java?
What is reflection in java and why is it useful?
what is multithreading in java?
What is the use of synchronization in Java?
What is Framework?
What are the testing Framework available in java?
Difference between jUnit and testNG?
What are the dependencies for this project?
what is dependency injection in java?
What is static binding and dynamic binding?