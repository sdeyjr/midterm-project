package datastructure;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

public class UseMap {
    public static Connection connect = null;
    public static Statement statement = null;
    public static PreparedStatement ps = null;
    public static ResultSet resultSet = null;


    public static void main(String[] args) throws Exception {
        /*
         * Demonstrate how to use Map that includes storing and retrieving elements.
         * Add List<String> into a Map. Like, Map<String, List<string>> list = new HashMap<String, List<String>>();
         * Use For Each loop and while loop with Iterator to retrieve data.
         *
         * Use MySql Database to store data and retrieve data.
         */

        Map<String, List<String>> sports = new HashMap<String, List<String>>();
        List<String> topsoccerteams = new ArrayList<>();
        topsoccerteams.add("Real Madrid");
        topsoccerteams.add("Liverpool");
        topsoccerteams.add("Man United");
        topsoccerteams.add("Man City");

        List<String> bottomsoccerteam = new ArrayList<String>();
        bottomsoccerteam.add("Barcalona");
        bottomsoccerteam.add("PSG");
        bottomsoccerteam.add("Napoli");
        bottomsoccerteam.add("Arsenal");


        sports.put("List of Top-Teams:", topsoccerteams);
        sports.put("List of Bottom-Teams:", bottomsoccerteam);
        System.out.println(sports);
        System.out.println(sports.size());
        System.out.println(sports.get("List of Top-Teams:"));
        System.out.println(sports.get("List of Bottom-Teams:"));
        System.out.println(sports.get("List of Top-Teams:").get(0));
        System.out.println(sports.get("List of Bottom-Teams:").get(0));
        sports.remove("List of Top-Teams:", topsoccerteams.remove(3));
        sports.remove("List of Bottom-Teams:", bottomsoccerteam.remove(3));
        System.out.println(sports.get("List of Top-Teams:"));
        System.out.println(sports.get("List of Bottom-Teams:"));

        System.out.println("***********************************");
        System.out.println("Using for each loop");
        System.out.println("***********************************");
        System.out.println("Top Teams: ");
        topsoccerteams.forEach(System.out::println);
        System.out.println("Bottom Teams: ");
        bottomsoccerteam.forEach(System.out::println);

        System.out.println("***********************************");
        System.out.println("Using while loop");
        System.out.println("***********************************");
        Iterator itr = sports.entrySet().iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        System.out.println("Inserting the data inside MySQL.");
        insertDataFromArrayListToSqlTable((ArrayList<String>) topsoccerteams, "midterm", "jawad");
        UseMap useMap = new UseMap();
        useMap.readDataBase("midterm", "jawad");
    }

    public static Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        InputStream ism = new FileInputStream("src/main/java/secret.properties");
        prop.load(ism);
        ism.close();
        return prop;
    }

    public static Connection connectToSqlDatabase() throws IOException, SQLException, ClassNotFoundException {
        Properties prop = loadProperties();
        String driverClass = prop.getProperty("MYSQLJDBC.driver");
        String url = prop.getProperty("MYSQLJDBC.url");
        String userName = prop.getProperty("MYSQLJDBC.userName");
        String password = prop.getProperty("MYSQLJDBC.password");
        Class.forName(driverClass);
        connect = DriverManager.getConnection(url, userName, password);
        System.out.println("Database is connected");
        return connect;
    }

    public List<String> readDataBase(String tableName, String columnName) throws Exception {
        List<String> data = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from " + tableName);
            data = getResultSetData(resultSet, columnName);
        } finally {
            close();
        }
        return data;
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    private List<String> getResultSetData(ResultSet resultSet2, String columnName) throws SQLException {
        List<String> dataList = new ArrayList<String>();
        while (resultSet.next()) {
            String itemName = resultSet.getString(columnName);
            dataList.add(itemName);
        }
        return dataList;
    }

    public static void insertDataFromArrayListToSqlTable(ArrayList<String> list, String tableName, String columnName) {
        try {
            connectToSqlDatabase();
            for (String st : list) {
                ps = connect.prepareStatement("INSERT INTO " + tableName + " ( " + columnName + " ) VALUES(?)");
                ps.setObject(1, st);
                ps.executeUpdate();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
