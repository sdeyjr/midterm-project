
package datastructure;

import java.util.ArrayList;
import java.util.LinkedList;

public class UseArrayList {

    public static void main(String[] args) {
        /*
         * Demonstrate how to use ArrayList that includes add,peek,remove,retrieve elements.
         * Use For Each loop and while loop with Iterator to retrieve data.
         *
         */
        ArrayList<LinkedList<String>> list = new ArrayList<>();
        LinkedList<String> list2 = new LinkedList<>();
        list2.add("hello");
        list2.add("hi");
        list2.add("bye");
        list2.add("goodbye");
        LinkedList<String> list3 = new LinkedList<>();
        list3.add("hello1");
        list3.add("hi1");
        list3.add("bye1");
        list3.add("goodbye1");
        LinkedList<String> list4 = new LinkedList<>();
        list4.add("hello2");
        list4.add("hi2");
        list4.add("bye2");
        list4.add("goodbye2");

        list.add(list2);
        list.add(list3);
        list.add(list4);
        System.out.println("Initial Linked list List: " + list2);
        System.out.println("Initial Array List" + list);

        System.out.println("Using For Loop:\n");
        for (int i = 0; i < 1; i++) {
            System.out.println("Peeking: " + list2.peek());
            System.out.println("Peeking first: " + list2.peekFirst());
            System.out.println("Peeking last: " + list2.peekLast());
        }

        System.out.println("\nUsing While Loop:\n");
        list.remove(1);
        int num = 0;
        while (1 > num) {
            System.out.println("After removing: " + list);
            System.out.println("The element is: " + list.get(1));
            num++;
        }
    }
}