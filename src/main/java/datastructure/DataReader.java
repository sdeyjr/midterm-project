package datastructure;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataReader {
    public static Connection connect = null;
    public static Statement statement = null;
    public static PreparedStatement ps = null;
    public static ResultSet resultSet = null;

    /*
     * User API to read the below textFile and print to console. /
     * Use BufferedReader class./
     * Use try....catch block to handle Exception./
     *
     * Use MySql Database to store data and retrieve data./
     *
     * After reading from file using BufferedReader API,/
     * store each line into ArrayList/
     * store each words into a table in the database./
     * Use For Each loop/while loop/Iterator to retrieve data./
     */
    // String textFile = System.getProperty("user.dir") + "/src/data/self-driving-car.txt";


    public static void main(String[] args) throws Exception {
        String textFile = getTextFile("src/main/java/datastructure/self-driving-car.txt");
        System.out.println(textFile);
        System.out.println("****************");
        System.out.println("Reading File line by line using BufferedReader");
        System.out.println("****************");

        FileInputStream rv25 = null;
        BufferedReader rv26 = null;
        try {
            rv25 = new FileInputStream("src/main/java/datastructure/self-driving-car.txt");
            rv26 = new BufferedReader(new InputStreamReader(rv25));
            String line = rv26.readLine();
            while (line != null) {
                System.out.println(line);
                line = rv26.readLine();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rv26.close();
                rv25.close();
            } catch (IOException ex) {
                Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("***********************************");
        System.out.println("Storing in ArrayList");
        System.out.println("***********************************");

        Scanner s = new Scanner(new File("src/main/java/datastructure/self-driving-car.txt"));
        ArrayList<String> rv30 = new ArrayList<String>();
        while (s.hasNextLine()) {
            rv30.add(s.nextLine());
        }
        s.close();
        for (int i = 0; i < rv30.size(); i++) {
            System.out.println(i + " :  " + rv30.get(i));
        }

        System.out.println("***********************************");
        System.out.println("Storing every word in ArrayList");
        System.out.println("***********************************");
        Scanner t = new Scanner(new File("src/main/java/datastructure/self-driving-car.txt"));
        ArrayList<String> rv32 = new ArrayList<String>();
        while (t.hasNext()) {
            rv32.add(t.next());
        }
        t.close();
        for (int i = 0; i < rv32.size(); i++) {
            System.out.println(i + " :  " + rv32.get(i));
        }
        insertDataFromArrayListToSqlTable(rv32, "midterm", "rahman");
        DataReader dataReader = new DataReader();
        dataReader.readDataBase("midterm", "rahman");
        dataReader.close();
    }

    public static String getTextFile(String filepath) throws IOException {
        StringBuilder finalText = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filepath));
        String tempContainer = "";
        while ((tempContainer = bufferedReader.readLine()) != null) {
            finalText.append(tempContainer);
        }
        return finalText.toString();
    }

    public static Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        InputStream ism = new FileInputStream("src/main/java/secret.properties");
        prop.load(ism);
        ism.close();
        return prop;
    }

    public static Connection connectToSqlDatabase() throws IOException, SQLException, ClassNotFoundException {
        Properties prop = loadProperties();
        String driverClass = prop.getProperty("MYSQLJDBC.driver");
        String url = prop.getProperty("MYSQLJDBC.url");
        String userName = prop.getProperty("MYSQLJDBC.userName");
        String password = prop.getProperty("MYSQLJDBC.password");
        Class.forName(driverClass);
        connect = DriverManager.getConnection(url, userName, password);
        System.out.println("Database is connected");
        return connect;
    }

    public List<String> readDataBase(String tableName, String columnName) throws Exception {
        List<String> data = new ArrayList<String>();

        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from " + tableName);
            data = getResultSetData(resultSet, columnName);
        } finally {
            close();
        }
        return data;
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    private List<String> getResultSetData(ResultSet resultSet2, String columnName) throws SQLException {
        List<String> dataList = new ArrayList<String>();
        while (resultSet.next()) {
            String itemName = resultSet.getString(columnName);
            dataList.add(itemName);
        }
        return dataList;
    }

    public static void insertDataFromArrayListToSqlTable(ArrayList<String> list, String tableName, String columnName) {
        try {
            connectToSqlDatabase();
            for (String st : list) {
                ps = connect.prepareStatement("INSERT INTO " + tableName + " ( " + columnName + " ) VALUES(?)");
                ps.setObject(1, st);
                ps.executeUpdate();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}