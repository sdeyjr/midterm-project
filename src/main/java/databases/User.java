package databases;

public class User {

    private String stName;
    private String stId;
    private String stDOB;

    public User() {
    }

    public User(String stName, String stID) {
        this.stName = stName;
        this.stId = stID;
    }

    public User(String stName, String stID, String stDOB) {
        this.stName = stName;
        this.stId = stID;
        this.stDOB = stDOB;
    }

    public String getStName() {
        return stName;
    }

    public void setStName(String stName) {
        this.stName = stName;
    }

    public String getStID() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getStDOB() {
        return stDOB;
    }

    public void setStDOB(String stDOB) {
        this.stDOB = stDOB;
    }
}
