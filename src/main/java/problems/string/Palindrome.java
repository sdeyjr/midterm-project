package problems.string;

public class Palindrome {
    public static void main(String[] args) {
        /*
          If a String is reversed and it remains unchanged, that is called Palindrome. For example, MOM,DAD,MADAM are
          Palindrome. So write java code to check if a given String is Palindrome or not.
         */

        //String s = "Mom" + "om";
        String s = "rotor";
        s = s.toLowerCase();//Converting the string into lowercase
        if (isPalindrome(s))
            System.out.print("It is a palindrome");
        else
            System.out.print("It is not a palindrome");
    }

    public static boolean isPalindrome(String swear2god) {    // This method returns true if string is a palindrome
        int i = 0, j = swear2god.length() - 1;        // Pointers pointing to the beginning and the end of the string
        while (i < j) {        // While there are characters to compare
            if (swear2god.charAt(i) != swear2god.charAt(j))   // If there is a mismatch
                return false;
            i++;            // Increment first pointer and
            j--;            // decrement the other
        }
        return true;
    }
}