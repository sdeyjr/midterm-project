package problems.string;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {

    public static void main(String[] args) {
        //Write a Java Program to check if the two String are Anagram. Two String are called Anagram when there is
        //same character but in different order.For example,"CAT" and "ACT", "ARMY" and "MARY".


        Scanner scanner = new Scanner(System.in);
        System.out.println("write first word: ");
        String str1 = scanner.nextLine();
        System.out.println("write second word: ");
        String str2 = scanner.nextLine();

        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();


        if (str1.length() == str2.length()) { // checking if length is same between both arrays


            char[] charArray1 = str1.toCharArray(); // converting strings to char arrays
            char[] charArray2 = str2.toCharArray();

            Arrays.sort(charArray1);             // sorting the char arrays
            Arrays.sort(charArray2);

            boolean result = Arrays.equals(charArray1, charArray2); // after sorting if the two arrays are same
            if (result) {
                System.out.println(str1 + " and " + str2 + " are anagram.");
            } else {
                System.out.println(str1 + " and " + str2 + " are not anagram.");
            }
        } else {
            System.out.println(str1 + " and " + str2 + " are not anagram.");
        }
    }
}
