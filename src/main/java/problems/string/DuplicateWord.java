package problems.string;

public class DuplicateWord {

    /* * Write a java program to find the duplicate words and their number of occurrences in the string.
     * Also Find the average length of the words.
     */

    public static void main(String[] args) {
        String s = "Java is a programming Language. Java is also an Island of Indonesia. Java is widely used language";
        int count;

        s = s.toLowerCase();//Converting the string into lowercase

        String shobdos[] = s.split(" "); //Splitting the string into words using built-in function

        System.out.println("Duplicate words and the number of occurance : ");

        for (int i = 0; i < shobdos.length; i++) {
            count = 1;
            for (int j = i + 1; j < shobdos.length; j++) {
                if (shobdos[i].equals(shobdos[j])) {
                    count++;
                    shobdos[j] = "0";  //Setting shobdos[j] to 0 to avoid printing visited word
                }
            }
            if (count > 1 && shobdos[i] != "0") // this displays the duplicate word if count is greater than 1
                System.out.println(shobdos[i] + " " + count);
        }


        String[] strArray = s.split(" ");
        int totalChars = 0; //change the datatype to float for decimal(more accurate) output
        for(String j : strArray){
            totalChars += j.length();
        }
        int words = strArray.length;
        int averageWordLength = (int)(totalChars/words);
        System.out.println("Average length of the words is " + averageWordLength + " letters");
    }
}