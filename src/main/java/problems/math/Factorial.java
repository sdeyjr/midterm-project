package problems.math;

public class Factorial {

    public static void main(String[] args) {
        /*
         * Factorial of 5! = 5 x 4 X 3 X 2 X 1 = 120.
         * Write a java program to find Factorial of a given number using Recursion as well as Iteration.
         *
         */

        //recursion = process in which a function calls itself

        int number = 5, result;
        result = factorial(number);
        System.out.println(number + "'s" + " factorial is " + result);
    }

    public static int factorial(int n) {
        if (n != 0)
            return n * factorial(n - 1); // recursive call
        else
            return 1;
    }
}

