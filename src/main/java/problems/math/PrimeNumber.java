package problems.math;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PrimeNumber {
    public static Connection connect = null;
    public static Statement statement = null;
    public static PreparedStatement ps = null;
    public static ResultSet resultSet = null;

    public static void main(String[] args) throws Exception {
        /*
         * Find list of Prime numbers from number 2 to 1 million.
         * Try the best solution as possible.Which will take less CPU life cycle.
         * Out put number of Prime numbers on the given range.
         *
         *
         * Use MySql Database to store data and retrieve data.
         *
         */
        numbers();
        ArrayList<Integer> primeNums = new ArrayList<>();
        primeNums.add(numbers());
        for (int i = 0; i < primeNums.size(); i++) {
            System.out.println(primeNums.get(i));
        }

        insertDataFromArrayListToSqlTable(primeNums, "midterm", "md");
        readDataBase("midterm", "md");
    }

    public static int numbers() {
        int max = 20;
        System.out.println("List of the prime number between 1 and " + max);
        int primeNumbers;
        int primes = 0;
        for (primeNumbers = 2; primeNumbers <= max; primeNumbers++) {
            boolean isPrime = true;
            for (int j = 2; j <= primeNumbers / 2; j++) {
                if (primeNumbers % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime == true) {
                System.out.println(primeNumbers);
            }
            primes = primeNumbers;
        }
        return primes;
    }


    public static Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        InputStream ism = new FileInputStream("src/main/java/secret.properties");
        prop.load(ism);
        ism.close();
        return prop;
    }

    public static Connection connectToSqlDatabase() throws IOException, SQLException, ClassNotFoundException {
        Properties prop = loadProperties();
        String driverClass = prop.getProperty("MYSQLJDBC.driver");
        String url = prop.getProperty("MYSQLJDBC.url");
        String userName = prop.getProperty("MYSQLJDBC.userName");
        String password = prop.getProperty("MYSQLJDBC.password");
        Class.forName(driverClass);
        connect = DriverManager.getConnection(url, userName, password);
        System.out.println("Database is connected");
        return connect;
    }

    public static void insertDataFromArrayListToSqlTable(ArrayList<Integer> list, String tableName, String columnName) {
        try {
            connectToSqlDatabase();
            for (int st : list) {
                ps = connect.prepareStatement("INSERT INTO " + tableName + " ( " + columnName + " ) VALUES(?)");
                ps.setObject(1, st);
                ps.executeUpdate();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getResultSetData(ResultSet resultSet2, String columnName) throws SQLException {
        List<String> dataList = new ArrayList<String>();
        while (resultSet.next()) {
            String itemName = resultSet.getString(columnName);
            dataList.add(itemName);
        }
        return dataList;
    }

    public static List<String> readDataBase(String tableName, String columnName) throws Exception {
        List<String> data = new ArrayList<String>();
        try {
            connectToSqlDatabase();
            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from " + tableName);
            data = getResultSetData(resultSet, columnName);
            System.out.println(data);
        } catch (ClassNotFoundException e) {
            throw e;
        } finally {
            //close();
        }
        return data;
    }
}
