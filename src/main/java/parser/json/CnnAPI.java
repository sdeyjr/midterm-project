package parser.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class CnnAPI {
    /*
      You can get API_KEY from this below link. Once you have the API_KEY, you can fetch the top-headlines news.
      https://newsapi.org/s/cnn-api

      Fetch This following CNN API, It will return some news in Json data. Store the data in the data.json file
      https://newsapi.org/v2/top-headlines?sources=cnn&apiKey=YOUR_API_KEY

	   Read the articles array and construct Headline news as

	   source,
	   author,
	   title,
	   description,
	   url,
	   urlToImage,
	   publishedAt
	   content.

	   You need to design News Data Model and construct headline news.
	   You can store in Map and then into ArrayList as your choice of Data Structure.

     */

    public static void main(String[] args) throws IOException, JSONException {
        JSONObject rootObject = new JSONObject(new String(Files.readAllBytes(new File("src/json/parser/data.json").toPath())));
        JSONArray array = rootObject.getJSONArray("articles");
        List<Articles> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            JSONObject dataJson = array.getJSONObject(i);
            ObjectMapper objectMapper = new ObjectMapper();
            Articles data = null;
            try {
                data = objectMapper.readValue(dataJson.toString(), Articles.class);
            } catch (JsonMappingException e) {
                e.printStackTrace();
            }
            list.add(data);
        }

        //continue


    }
    /*public static void main(String[] args) throws MalformedURLException, IOException {
        String sURL = "https://newsapi.org/v2/top-headlines?sources=cnn&apiKey=0a0c04a6b00e4d4e9b68c9080a6dfee6";
        URL url = new URL(sURL);
        List<Articles> articlesList = new ArrayList<>();
        Articles articles = new Articles();
        URLConnection request = url.openConnection();
        request.connect();
        JsonArray jsonArray = null;
        JsonObject rootObj = null;
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        if (root instanceof JsonObject) {
            rootObj = root.getAsJsonObject();
        } else if (root instanceof JsonArray) {
            jsonArray =  root.getAsJsonArray();
        }
        jsonArray = rootObj.getAsJsonArray("articles");
        for (int i = 0; i < jsonArray.size(); i++) {
            try {
                JsonObject jsonobject = jsonArray.get(i).getAsJsonObject();
                //you code start here
                articles.setAuthor(jsonobject.get("author").toString());
                articles.setTitle(jsonobject.get("title").toString());
                articles.setDescription(jsonobject.get("author").toString());
                articles.setContent(jsonobject.get("content").toString());
                articles.setContent(jsonobject.get("url").toString());
                articles.setContent(jsonobject.get("urlToImage").toString());
                articles.setContent(jsonobject.get("publishedAt").toString());
                articlesList.add(new Articles(articles.getAuthor(), articles.getTitle(), articles.getDescription(), articles.getContent(), articles.getUrl(), articles.getUrlToImage(), articles.getPublishedAt()));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
        for (Articles a : articlesList) {
            System.out.println(a.getAuthor()+" "+a.getTitle()+" "+a.getDescription()+" "+a.getContent()+" "+a.getUrl()+" "+a.getUrlToImage()+" "+a.getPublishedAt());
        }
        try {
            FileWriter file = new FileWriter("src/databases/data.json");
            file.write(rootObj.toString());
            file.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }
*/
}