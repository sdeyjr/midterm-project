package design;

public abstract class EmployeeAbstract implements Employee {

    public abstract int calculateSalary(int salary);

    abstract int employeePerformance ();

}
